const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const cfg = require('./config.js');

const app = express();
const router = express.Router();
router.use(bodyParser.json());

function getTest(name){
	return new Promise((resolve, reject) => {
		fs.readFile('tests/' + name + '.json', 'utf8', function(err, data){
			err ? reject(err) : resolve(JSON.parse(data));
		});
	});
}

function saveTest(name, data){
	return new Promise((resolve, reject) => {
		fs.writeFile('tests/' + name + '.json', JSON.stringify(data), 'utf8', err => {
			err ? reject(err) : resolve();
		});
	});
}

function delTest(name){
	return new Promise((resolve, reject) => {
		fs.unlink('tests/' + name + '.json', err => {
			err ? reject(err) : resolve();
		});
	});
}

function saveAnswers(name, answers, agent, ip){
	return new Promise((resolve, reject) => {
		fs.readFile('results/' + name + '.json', 'utf8', function(err, data){
			if(err){
				data = [];
			}
			else{
				data = JSON.parse(data);
			}
			data.push({
				ip,
				agent,
				answers,
			});
			fs.writeFile('results/' + name + '.json', JSON.stringify(data), 'utf8', err => {
				err ? reject(err) : resolve();
			});
		});
	});
}

router.route('/').get((req, res) => {
	let result = [];
	fs.readdir('tests', (err, files) => {
		for(let i = 0, l = files.length; i < l; ++i){
			if(~files[i].indexOf('.json')){
				result.push(files[i].substr(0, files[i].length - 5));
			}
		}
		res.status(200);
		res.json(result);
	});
});

router.route('/result/:name').post((req, res) => {
	saveAnswers(req.params.name, req.body, req.get('User-Agent'), req.headers['x-forwarded-for'] || req.connection.remoteAddress).then(() => {
		res.status(200);
		res.json({});
	}, err => {throw err;});
});

router.route('/:name')
.get((req, res) => {
	getTest(req.params.name).then(test => {
		res.status(200);
		res.json(test);
	}, err => {throw err;});
})
.post((req, res) => {
	if(!req.query.token || req.query.token != '123'){
		res.redirect(cfg.redirect);
		return;
	}
	saveTest(req.params.name, req.body).then(() => {
		res.status(200);
		res.json({});
	}, err => {throw err;});
})
.delete((req, res) => {
	if(!req.query.token || req.query.token != '123'){
		res.redirect(cfg.redirect);
		return;
	}
	delTest(req.params.name).then(() => {
		res.status(200);
		res.json({});
	}, err => {throw err;});
});


app.use('/api', router);

app.listen(cfg.apiPort);

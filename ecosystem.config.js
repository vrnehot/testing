module.exports = {
	apps: [
		{
			name: 'tapi',
			script: './api.js',
			env: {
				COMMON_VARIABLE: 'true'
			},
			env_production : {
				NODE_ENV: 'production'
			},
			log_date_format: 'YYYY-MM-DD HH:mm Z',
			log_type: 'json',
		},
		{
			name: 'testing',
			script: './index.js',
			log_date_format: 'YYYY-MM-DD HH:mm Z',
			log_type: 'json',
		},
	],
};

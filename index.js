const fs = require('fs');
const express = require('express');
const cors = require('cors')
const app = express();
const cfg = require('./config.js');

const httpProxy = require('http-proxy');
const apiProxy = httpProxy.createProxyServer();


app.use(cors());

app.use('/dist', express.static('dist'));

app.all(['/api','/api/*'], (req, res) => {
	res.setHeader('Last-Modified', (new Date()).toUTCString());
	apiProxy.web(req, res, {target: 'http://localhost:' + cfg.apiPort});
});

app.get('*', (req, res) => {
	if(!req.query.token || req.query.token != '123'){
		res.redirect(cfg.redirect);
		return;
	}
	res.end(fs.readFileSync('./index.html', 'utf-8'));
});

app.listen(cfg.port);

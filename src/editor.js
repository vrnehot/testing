import 'vuetify/dist/vuetify.min.css'

import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './Editor.vue'
// import store from './store'
import router from './router/editor.js'

Vue.use(Vuetify)

new Vue({
	el: '#app',
	// store,
	router,
	render: h => h(App)
});

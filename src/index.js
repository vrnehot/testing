import 'vuetify/dist/vuetify.min.css'

import 'babel-polyfill'

import Vue from 'vue'
import Vuetify from 'vuetify'
// import {
// 	Vuetify,
// 	VApp,
// 	VList,
// 	VIcon,
// 	VBtn,
// 	VCard,
// 	VCheckbox,
// 	VProgressCircular,
// } from 'vuetify'
// import directives from 'vuetify/es5/directives'

Vue.use(Vuetify)
// Vue.use(Vuetify, {
// 	components: {
// 		VApp,
// 		VList,
// 		VIcon,
// 		VBtn,
// 		VCard,
// 		VCheckbox,
// 		VProgressCircular,
// 	},
// 	directives,
// });

import App from './Viewer.vue'

let id = 'ehot-testing';
let script = document.getElementById(id);
let app = document.createElement('div');
app.id = 'ehot-testing-app';
script.parentNode.insertBefore(app, script);
console.log('testing ready');

new Vue({
	el: app,
	render: h => h(App),
});

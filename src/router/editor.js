import Vue from 'vue'
import VueRouter from 'vue-router'

import List from '../pages/List.vue'
import Form from '../pages/Form.vue'

Vue.use(VueRouter);

export default new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/',
			component: List,
			meta: {
				title: 'Список тестов'
			},
		},
		{
			path: '/:name',
			component: Form,
			meta: {
				title: 'Редактирование теста'
			},
		},
	],
});
